// swift-tools-version:5.4
import PackageDescription


let package = Package(
	name: "music-cleanup",
	platforms: [.macOS(.v11)],
	dependencies: [
		.package(url: "https://github.com/apple/swift-argument-parser.git", from: "0.4.3"),
		.package(url: "https://github.com/apple/swift-system.git",          from: "0.0.2"),
		.package(url: "https://github.com/xcode-actions/clt-logger.git",    from: "0.3.4"),
		.package(url: "https://github.com/xcode-actions/XcodeTools.git",    from: "0.3.5")
	],
	targets: [
		.executableTarget(name: "music-cleanup", dependencies: [
			.product(name: "ArgumentParser", package: "swift-argument-parser"),
			.product(name: "CLTLogger",      package: "clt-logger"),
			.product(name: "SystemPackage",  package: "swift-system"),
			.product(name: "XcodeTools",     package: "XcodeTools")
		])
	]
)
