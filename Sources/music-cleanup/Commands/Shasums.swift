import CryptoKit
import Foundation

import ArgumentParser



struct Shasums : ParsableCommand {
	
	@OptionGroup
	var commonOptions: CommonOptions
	
	func run() throws {
		let mediaFiles = try Music.listMediaFiles(inMediaRoot: commonOptions.libraryMediaRoot, pkgExts: Set(commonOptions.packageExtensions), cacheFile: commonOptions.mediaFilesListCachePath)
		for file in mediaFiles {
			autoreleasepool{
				guard let fileContents = try? Data(contentsOf: URL(fileURLWithPath: file)) else {
					print("<read error>: \(file)")
					return
				}
				print("\(SHA256.hash(data: fileContents).reduce("", { $0 + String(format: "%02x", $1) })): \(file)")
			}
		}
	}
	
}
