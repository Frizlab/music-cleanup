import Foundation

import ArgumentParser



struct CommonOptions : ParsableArguments {
	
	@Option(help: "The Media folder in the Music library. Must end with a “/”.")
	var libraryMediaRoot: String
	
	@Option(help: "The extensions of the folders that should be considered packages (files inside are not considered).")
	var packageExtensions: [String] = ["movpkg", "itlp"]
	
	@Option(help: "Path to a file containing a cached version of the list of files inside theh media folder.")
	var mediaFilesListCachePath: String?
	
	func validate() throws {
		guard libraryMediaRoot.hasSuffix("/") else {
			throw ValidationError("The library media root must end with a “/”.")
		}
	}
	
}
