import Foundation

import ArgumentParser
import CLTLogger
import Logging



struct Analyse : ParsableCommand {
	
	@OptionGroup
	var commonOptions: CommonOptions
	
	@Option
	var xmlLibraryExport: String
	
	@Flag
	var fixDuplicates: Bool = false
	
	@Flag
	var yes: Bool = false
	
	@Option
	var backupFolder: String!
	
	func run() throws {
		/* TODO: When possible (async/await support), use @TaskLocal */
		Logging.LoggingSystem.bootstrap{ _ in CLTLogger() }
		let logger = { () -> Logger in
			var l = Logger(label: "music-cleanup")
			l.logLevel = .debug
			return l
		}()
		
		let mediaFilesSorted = try Music.listMediaFiles(inMediaRoot: commonOptions.libraryMediaRoot, pkgExts: Set(commonOptions.packageExtensions), cacheFile: commonOptions.mediaFilesListCachePath)
		let tracksFilesSorted = try Music.getFilePaths(fromTracks: Music.getTracks(inLibraryExport: xmlLibraryExport))
		let duplicates = Music.getDuplicates(fromSortedPaths: mediaFilesSorted)
		let tracksFiles = Set(tracksFilesSorted)
		let mediaFiles = Set(mediaFilesSorted)
		
		print("---")
		let notInMusicPrefix = tracksFiles.filter{ !$0.hasPrefix(commonOptions.libraryMediaRoot) }
		print("not-in-music-prefix-count: \(notInMusicPrefix.count)")
		print("not-in-music-prefix:")
		for file in notInMusicPrefix.sorted() {
			print("   - \(file)")
		}
		
		print("---")
		let onlyInDb = tracksFiles.subtracting(mediaFiles)
		print("only-in-db-count: \(onlyInDb.count)")
		print("only-in-db:")
		for file in onlyInDb.sorted() {
			print("   - \(file)")
		}
		
		print("---")
		let onlyOnDisk = mediaFiles.subtracting(tracksFiles)
		print("only-on-disk-count: \(onlyOnDisk.count)")
		print("only-on-disk:")
		for file in onlyOnDisk.sorted() {
			print("   - \(file)")
		}
		
		print("---")
		print("on-disk-duplicates-count: \(duplicates.count)")
		print("on-disk-duplicates:")
		for (idx, group) in duplicates.enumerated() {
			print("   \(idx):")
			for file in group.sorted() {
				print("      - \(file)")
			}
		}
		
		if fixDuplicates {
			guard let backupFolder = backupFolder else {
				throw ValidationError("backup-folder is required when trying to fix music")
			}
			
			if !yes {
				/* Confirm we want the fix */
				print()
				print("Try and fix this? ", terminator: "")
				var lineData = Data()
				while let charData = try FileHandle.standardInput.read(upToCount: 1), charData != Data("\n".utf8) {
					lineData += charData
				}
				guard let line = String(data: lineData, encoding: .utf8) else {
					throw ExitCode(42)
				}
				guard line.first?.lowercased() == "y" else {
					throw ExitCode(0)
				}
			}
			
			/* Let’s do this */
			logger.info("It’s go time!")
			/* For all files only on disk, we try and find the duplicate group that
			 * contains it. If there is a duplicate group for the file, we check
			 * there are only two files in the group. Of the two files, we apply a
			 * set of heuristics to choose the one we want in the Library:
			 *    - In Apple Music, take the more recent;
			 *    - In Music, take the least recent!
			 * Always check the replacement is either bigger or have the same size. */
			for (idx, file) in onlyOnDisk.sorted().enumerated() {
				autoreleasepool{
					if idx % 100 == 0 {
						logger.info("\(idx) items treated...")
					}
					let groups = duplicates.filter{ $0.contains(file) }
					guard let group = groups.first, groups.count == 1 else {
						if groups.isEmpty {
							logger.warning("Skipping \(file) because there are no duplicate groups containing it.")
						} else {
							logger.error("Skipping \(file) because there are more than one duplicate group containing it! This should not happen.")
						}
						return
					}
					guard group.count == 2 else {
						logger.warning("Skipping group for file \(file) because it contains \(group.count) elements. Only know how to treat couples.")
						return
					}
					
					let groupNoCurrent = group.subtracting([file])
					assert(groupNoCurrent.count == 1)
					
					let fileNotInLib = file
					let fileInLib = groupNoCurrent.randomElement()!
					
					let urlInLib = URL(fileURLWithPath: fileInLib)
					let urlNotInLib = URL(fileURLWithPath: fileNotInLib)
					
					let resVal = Set(arrayLiteral: URLResourceKey.fileSizeKey, .creationDateKey)
					guard
						let resValNotInLib = try? urlNotInLib.resourceValues(forKeys: resVal),
						let resValInLib = try? urlInLib.resourceValues(forKeys: resVal),
						let creationNotInLib = resValNotInLib.creationDate,
						let creationInLib = resValInLib.creationDate,
						let sizeNotInLib = (commonOptions.packageExtensions.contains(urlNotInLib.pathExtension) ? Int.max : resValNotInLib.fileSize),
						let sizeInLib = (commonOptions.packageExtensions.contains(urlInLib.pathExtension) ? Int.max : resValInLib.fileSize)
					else {
						logger.error("Cannot get file size or creation date of either \(fileInLib) or \(fileNotInLib).")
						return
					}
					
					let fileToKeep: String
					let fileToDelete: String
					let sizeToKeep: Int
					let sizeToDelete: Int
					if fileInLib.hasPrefix(commonOptions.libraryMediaRoot + "Music/") {
						assert(fileNotInLib.hasPrefix(commonOptions.libraryMediaRoot + "Music/"))
						/* In the Music folder, we take the oldest file. */
						if creationInLib < creationNotInLib {
							fileToKeep = fileInLib
							fileToDelete = fileNotInLib
							sizeToKeep = sizeInLib
							sizeToDelete = sizeNotInLib
							logger.warning("Keeping file \(fileToKeep) which is already in lib, which is unexpected for a Music case.")
						} else {
							fileToKeep = fileNotInLib
							fileToDelete = fileInLib
							sizeToKeep = sizeNotInLib
							sizeToDelete = sizeInLib
						}
					} else if fileInLib.hasPrefix(commonOptions.libraryMediaRoot + "Apple Music/") || fileInLib.hasPrefix(commonOptions.libraryMediaRoot + "TV Shows/") {
						assert(fileNotInLib.hasPrefix(commonOptions.libraryMediaRoot + "Apple Music/") || fileNotInLib.hasPrefix(commonOptions.libraryMediaRoot + "TV Shows/"))
						/* In the Apple Music and TV Shows folders, we take the newest file. */
						if creationInLib >= creationNotInLib {
							fileToKeep = fileInLib
							fileToDelete = fileNotInLib
							sizeToKeep = sizeInLib
							sizeToDelete = sizeNotInLib
						} else {
							fileToKeep = fileNotInLib
							fileToDelete = fileInLib
							sizeToKeep = sizeNotInLib
							sizeToDelete = sizeInLib
							logger.warning("Keeping file \(fileToKeep) which is not in lib, which is unexpected for an Apple Music or TV Shows case.")
						}
					} else {
						logger.error("Unknown file prefix for file \(fileInLib).")
						return
					}
					do {
						if
							sizeToKeep >= sizeToDelete ||
							movingToMp3FromSomethingElse(fileToKeep, fileToDelete) ||
							(try? exactStreamInKeptFile(fileToKeep, fileToDelete)) == true
						{
							/* Sane case: file to remove is bigger than file to delete, or
							 * other rules. */
							logger.debug("Removing \(fileToDelete)")
							try FileManager.default.removeItem(atPath: fileToDelete)
						} else {
							logger.warning("File to keep (\(fileToKeep)) is smaller than file to delete (\(fileToDelete)). Backing up file to delete.")
							let rootRangeInFileToDelete = fileToDelete.range(of: commonOptions.libraryMediaRoot, options: [.literal, .anchored])!
							let relativePathToDelete = { () -> String in
								var str = fileToDelete
								str.removeSubrange(rootRangeInFileToDelete)
								return str
							}()
							let moveSource = URL(fileURLWithPath: fileToDelete)
							let moveDestination = URL(fileURLWithPath: relativePathToDelete, relativeTo: URL(fileURLWithPath: backupFolder, isDirectory: true))
							logger.debug("Moving \(moveSource.path) to \(moveDestination.path)")
							try FileManager.default.createDirectory(at: moveDestination.deletingLastPathComponent(), withIntermediateDirectories: true, attributes: nil)
							try FileManager.default.moveItem(at: moveSource, to: moveDestination)
						}
						if fileToKeep != fileInLib {
							logger.debug("Moving \(fileToKeep) to \(fileInLib)")
							try FileManager.default.moveItem(atPath: fileToKeep, toPath: fileInLib)
						}
					} catch {
						logger.error("Cannot (re)move \(fileToDelete) or move \(fileToKeep): \(error)")
					}
				}
			}
		}
	}
	
	func movingToMp3FromSomethingElse(_ fileToKeep: String, _ fileToDelete: String) -> Bool {
		return fileToKeep.lowercased().hasSuffix(".mp3") && !fileToDelete.lowercased().hasSuffix(".mp3")
	}
	
	func exactStreamInKeptFile(_ fileToKeep: String, _ fileToDelete: String) throws -> Bool {
		let keptStreams    = try Process.executeAndGrep(executable: "/usr/local/bin/ffprobe", arguments: ["-i", fileToKeep],   grepString: "Stream #")
		let deletedStreams = try Process.executeAndGrep(executable: "/usr/local/bin/ffprobe", arguments: ["-i", fileToDelete], grepString: "Stream #")
		return Set(keptStreams).isSuperset(of: Set(deletedStreams))
	}
	
}
