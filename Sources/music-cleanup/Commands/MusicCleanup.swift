import Foundation

import ArgumentParser



@main
struct MusicCleanup : ParsableCommand {
	
	@OptionGroup
	var commonOptions: CommonOptions
	
	static var configuration: CommandConfiguration = .init(subcommands: [
		Analyse.self,
		Shasums.self
	])
	
}
