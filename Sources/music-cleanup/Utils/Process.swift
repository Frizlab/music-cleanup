import Foundation

import SystemPackage
import XcodeTools



extension Process {
	
	struct ProcessExitedInUnexpectedWay : Error {}
	
	static func executeAndGrep(executable: String, arguments: [String], grepString: String) throws -> [String] {
		var matchingLines = [String]()
		let outputHandler: (String, FileDescriptor) -> Void = { line, fd in
			let line = line.trimmingCharacters(in: .newlines)
			guard line.range(of: grepString) != nil else {return}
			matchingLines.append(line)
		}
		let (exitCode, exitReason) = try Process.spawnAndStream(
			executable, args: arguments,
			stdin: nil, stdoutRedirect: .capture, stderrRedirect: .capture,
			outputHandler: outputHandler
		)
		guard exitReason == .exit, exitCode == 0 else {
			throw ProcessExitedInUnexpectedWay()
		}
		return matchingLines
	}
	
}
