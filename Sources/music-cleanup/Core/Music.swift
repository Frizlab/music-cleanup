import Foundation



enum Music {
	
	struct InvalidURLInDb : Error {}
	struct UnexpectedTypeInDb : Error {}
	
	static func getTracks(inLibraryExport libraryPath: String) throws -> [[String: Any]] {
		let data = try Data(contentsOf: URL(fileURLWithPath: libraryPath))
		guard
			let db = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [String: Any],
			let tracksByID = db["Tracks"] as? [String: [String: Any]]
		else {
			throw UnexpectedTypeInDb()
		}
		return Array(tracksByID.values).filter{ $0["Playlist Only"] as? Bool != true }
	}
	
	static func tracksWithNoLocation(fromTracks tracks: [[String: Any]]) -> [[String: Any]] {
		return tracks.filter{ $0["Location"] == nil }
	}
	
	static func getFilePaths(fromTracks tracks: [[String: Any]]) throws -> [String] {
		return try tracks.compactMap{ $0["Location"] }.map{ location -> String in
			guard let urlString = location as? String else {
				throw UnexpectedTypeInDb()
			}
			return urlString
		}.map{
			guard let url = URL(string: $0) else {
				throw InvalidURLInDb()
			}
			return url.path
		}
	}
	
	/* TODO: When we can, use async. */
	static func listMediaFiles(inMediaRoot mediaRoot: String, pkgExts: Set<String>, cacheFile: String?) throws -> [String] {
		if let cacheFile = cacheFile, let c = try? String(contentsOf: URL(fileURLWithPath: cacheFile)) {
			return c.split(separator: "\n").map(String.init)
		} else {
			let filesOnHDSorted = try! FileManager.default.subpathsOfDirectory(atPath: mediaRoot).map{ mediaRoot + $0 }.sorted()
			
			var latestFolderPackage: String?
			var filesOnHDFiltered = [String]()
			for (idx, file) in filesOnHDSorted.enumerated() {
				/* Filter .DS_Store */
				guard !file.hasSuffix("/.DS_Store") && !file.hasSuffix(".strings") else {
					continue
				}
				
				/* Filter directories */
				let nextIdx = filesOnHDSorted.index(after: idx)
				if nextIdx != filesOnHDSorted.endIndex {
					let nextFile = filesOnHDSorted[nextIdx]
					if nextFile.hasPrefix(file) {
						/* file is a folder; let’s check if it’s a package */
						if pkgExts.contains(where: { file.hasSuffix("." + $0) }) {
							latestFolderPackage = file
							filesOnHDFiltered.append(file)
						}
						continue
					}
				}
				/* Skip packages content */
				if let latestFolderPackage = latestFolderPackage, file.hasPrefix(latestFolderPackage) {
					continue
				}
				latestFolderPackage = nil
				filesOnHDFiltered.append(file)
			}
			if let cacheFile = cacheFile {
				_ = try? Data(filesOnHDFiltered.joined(separator: "\n").utf8).write(to: URL(fileURLWithPath: cacheFile))
			}
			return filesOnHDFiltered
		}
	}
	
	static func getDuplicates(fromSortedPaths paths: [String]) -> [Set<String>] {
		var previousNoExt: String?
		var curGroup = Set<String>()
		var groups = [Set<String>]()
		for file in paths.reversed() {
			let fileNoExt = (file as NSString).deletingPathExtension
			defer {previousNoExt = fileNoExt}
			
			if let previousNoExt = previousNoExt, fileNoExt.hasPrefix(previousNoExt) {
				curGroup.insert(file)
			} else {
				groups.append(curGroup)
				curGroup = [file]
			}
		}
		groups.append(curGroup)
		assert(paths.count == groups.reduce(0, { $0 + $1.count }))
		return groups.filter{ $0.count > 1 }
	}
	
}
